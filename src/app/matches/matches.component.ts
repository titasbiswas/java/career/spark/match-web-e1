import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Match } from '../_models/Match';
import { FilterCriteria } from '../_models/FilterCriteria';
import { MatchService } from '../_services/match.service';
import { Router } from '@angular/router';
import { UiServiceService } from '../ui-service.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent {

  currentMatch: Match;
  matches: Match[];
  hiddenMatches: Match[];
  filter: FilterCriteria = new FilterCriteria();
  ageRange: number[] = [18, 95];
  csRange: number[] = [1, 99];
  heightRange: number[] = [135, 210];
  dist: number = 300;

  showMenu = false;
  darkModeActive: boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
    private matchService: MatchService,
    private router: Router,
    public ui: UiServiceService) { }

  ngOnInit(): void {

    this.currentMatch = JSON.parse(localStorage.getItem('currentMatch'));
    this.loadInitialMatches();

    this.ui.darkModeState.subscribe((value) => {
      this.darkModeActive = value;
    });
    this.modeToggleSwitch();
  }

  toggleMenu() {
    this.showMenu = !this.showMenu;
  }

  modeToggleSwitch() {
    this.ui.darkModeState.next(!this.darkModeActive);
  }

  loadInitialMatches(): void {
    //let fc: FilterCriteria = new FilterCriteria();
    if (this.currentMatch.id) {
      this.filter.currentId = this.currentMatch.id;
    } else {
      this.filter.currentId = 0;
    }
    this.filter.age.min = this.ageRange[0];
    this.filter.age.max = this.ageRange[1];
    this.filter.compatibilityScore.min = this.csRange[0];
    this.filter.compatibilityScore.max = this.csRange[1];
    this.filter.height.min = this.heightRange[0];
    this.filter.height.max = this.heightRange[1];
    this.filter.locDist.distanceInKm = this.dist;
    this.filter.locDist.lat = this.currentMatch.city.lat;
    this.filter.locDist.lon = this.currentMatch.city.lon;
    this.filterMatches(null, null);
  }

  filterMatches(event: any, type: string): void {
    console.log(this.filter);
    this.filter.currentId = this.currentMatch.id;
    if (type === 'age') {
      this.filter.age.min = event[0];
      this.filter.age.max = event[1];
    } else if (type === 'cs') {
      this.filter.compatibilityScore.min = event[0];
      this.filter.compatibilityScore.max = event[1];
    } else if (type === 'height') {
      this.filter.height.min = event[0];
      this.filter.height.max = event[1];
    } else if (type === 'dist') {
      this.filter.locDist.distanceInKm = event;
      this.filter.locDist.lat = this.currentMatch.city.lat;
      this.filter.locDist.lon = this.currentMatch.city.lon;
    }
    this.matchService.getMatches(this.filter).subscribe(data => {
      this.matches = data.filter(match => !match.hidden);
      this.hiddenMatches = data.filter(match => match.hidden);
    });
  }

  returnToLanding(): void {

    this.router.navigate(['landing']);
  }

  toogleFav(event: any, match: Match): void {
    match.favourite = !match.favourite;
    this.matchService.alterMatches(match).subscribe(() => {
      this.filterMatches(null, null);
    });
  }

  toogleHideMath(event: any, match: Match): void {
    match.hidden = !match.hidden;
    this.matchService.alterMatches(match).subscribe(() => {
      this.filterMatches(null, null);
    });
  }

  sendSmiley(event: any, match: Match): void {
    match.contactsExchanged++;
    this.matchService.alterMatches(match).subscribe(() => {
      this.filterMatches(null, null);
    });
  }

  }
